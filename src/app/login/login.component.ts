import { Component, OnInit } from '@angular/core';
import {AuthService } from '../service/auth.service';
import {Auth2Service } from '../auth2.service';
import {Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(  public authService: AuthService,
    public router: Router,
    public auth2: Auth2Service) { }

  ngOnInit() {
  }
  
  /*onClickGoogleLogin() {
    this.authService.loginGoogle()
     .then((res) => {
       this.authService.initClient();
          
         this.router.navigate(['/home']);
     }).catch( err => console.log(err.message));
   }*/
   logueo(){
     this.authService.logueo()
       
       
   }
   SigIn(){
         this.auth2.SigIn();
   }

   

   listFile() {
        var request = gapi.client.drive.files.list({
            'pageSize': 100,
            'fields': "nextPageToken, files(id, name, mimeType, modifiedTime, size)",
            "q": "root in parents and trashed = false"
          });

          request.execute(function(resp) {
            //this.appendPre('Files:');
            var files = resp.result.files;
            if (files && files.length > 0) {
              for (var i = 0; i < files.length; i++) {
                var file = files[i];
                console.log(file);
                //this.appendPre(file.name + ' (' + file.id + ')');
              }
            } else {
              this.appendPre('No files found.');
            }
          });
  }

   signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
  }

}
