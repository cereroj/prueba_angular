import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular';

 

    onLoadCallback() {

      console.log('onLoadCallback');
      gapi.load('client:auth2', function() {
        gapi.client.init({
            apiKey : 'AIzaSyAKD0aLnfXDitwbNrLgeIBFyu37BYZMbl8',
            clientId: '769860705559-9t79m81968o50cpvrpkppq0qmsmf63rd.apps.googleusercontent.com',
            //This two lines are important not to get profile info from your user
            scope: 'https://www.googleapis.com/auth/drive',
            discoveryDocs : ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest']
        });        
      });     
    }
}
