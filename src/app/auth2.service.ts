import { Injectable,NgZone ,OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Auth2Service implements OnInit {
auth2: any;
userAuthToken = null;
User = {
	Name : "",
	Email : "",
	Picture : null
}
  googleAuth: gapi.auth2.GoogleAuth;
  constructor(private _zone: NgZone) { }


  ngOnInit() {
  }

  

	SetValores(valor:any){
   		this.User.Name = valor.ig;
   		this.User.Email = valor.U3;
       this.User.Picture = valor.Paa;
       console.log(this.User);
   }

 
//////////////////////////////////////////////////////////77
  SigIn(){
     console.log('I am passing signIn')

        var auth2 = gapi.auth2.getAuthInstance();         
          // Sign the user in, and then retrieve their ID.
          auth2.signIn().then(()=> {
          	var valores = auth2.currentUser.get().getBasicProfile(); 
            this.SetValores(valores);
            this.listFile();  
                      
          });
   }

   

   GetValores(){
   	return this.User;
   }

  signOut() {
    this.auth2 = gapi.auth2.getAuthInstance();

    this.auth2.signOut().then(() => {
      console.log('User signed out.');
       this._zone.run(() => {
        this.userAuthToken = null;
        
      });
    });

  }

 listFile() {
  gapi.client.load('drive' ,"v3",function() {
   
       gapi.client.drive.files.list({
          'pageSize': 10,
          'fields': "nextPageToken, files(id, name)"
        }).then(function(response) {
         // appendPre('Files:');
          var files = response.result.files;
          if (files && files.length > 0) {
            for (var i = 0; i < files.length; i++) {
              var file = files[i];
              console.log(file.name + ' (' + file.id + ')');
            }
          } else {
            console.log('No files found.');
          }
        });
      });
    }
}
