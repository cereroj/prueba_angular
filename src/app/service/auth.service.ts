import { Injectable,OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import {Router } from '@angular/router';
const CLIENT_ID = "1045027378677-6knfmkogc5nngs9c9hdf0k6dtcu2gd5g.apps.googleusercontent.com";
const API_KEY = "CTmM0KzQg_tzELBAEAp-oI9E";
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];
var SCOPES = 'https://www.googleapis.com/auth/drive';
import { HttpClientModule ,HttpHeaders} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {

  googleAuth: gapi.auth2.GoogleAuth;
  User = {
  Name : "",
  Email : "",
  Picture : null,
  status : false
}
  constructor(public afAuth: AngularFireAuth,public router: Router, public http :HttpClientModule) { }

  
  ngOnInit(){
  

  }

  SetValores(valor:any){
       this.User.Name = valor.ig;
       this.User.Email = valor.U3;
       this.User.Picture = valor.Paa;
       console.log(this.User);
   }

  initClient(){
    
      return gapi.client.init({
          apiKey:'AIzaSyAKD0aLnfXDitwbNrLgeIBFyu37BYZMbl8',
          clientId:'769860705559-9t79m81968o50cpvrpkppq0qmsmf63rd.apps.googleusercontent.com',
          discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest'],
          scope: 'https://www.googleapis.com/auth/drive',
      }).then(() => {  
        console.log("Exit Init"); 
          gapi.auth2.getAuthInstance().signIn();    
           //gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus);
          // Handle the initial sign-in state.
         // this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
     
     });
   
  }
    logueo() {  
       //gapi.load('client:auth2', this.initClient);
     
                
       
       this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
       //this.SetValores(gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile());
      
       
        
   }

    updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
          this.User.status = true;
        } else {
          this.User.status = false;
        }
        console.log( this.User.status);
      }

     listFiles() {
       gapi.load('drive',()=> {
          gapi.client.drive.files.list({
          'pageSize': 100,
          'fields': "nextPageToken, files(id, name)"
          }).then(function(response) {
            //appendPre('Files:');
            var files = response.result.files;
            if (files && files.length > 0) {
              for (var i = 0; i < files.length; i++) {
                var file = files[i];
                console.log(file.name + ' (' + file.id + ')');
              }
            } else {
              console.log('No files found.');
            }
          });
        });
      }

      
  }
